<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('components.head')
</head>

<body class="login-body">
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                @yield('content')
            </div>
        </section>
    </div>
    @include('components.script')
</body>

</html>