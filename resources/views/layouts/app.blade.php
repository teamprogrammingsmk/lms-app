<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('components.head')
</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            @include('components.navbar')

            @include('components.sidebar')

            @yield('content')

            @include('components.footer')
        </div>
    </div>
    
    @include('components.script')
    <script type="text/javascript">
         $('.custom-file-input').on('change',function(){
            //get the file name
            var fileName = $(this).val();
            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fileName);
        })
    </script>
</body>
</html>