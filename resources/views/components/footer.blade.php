<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
    </div>
    <div class="footer-right">
        Made with <i class="fas fa-heart text-danger"></i> by <a href="{{ route('author') }}">RPL 2018</a>
    </div>
</footer>
