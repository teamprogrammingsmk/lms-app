@extends('layouts.app')

@section('title', 'Post')

@section('content')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
            <div class="section-header">
                <h1>@yield('title')</h1>
            </div>
            @include('components.message')
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Judul</th>
                                        <th>Tanggal</th>
                                        <th>Lainnya</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            </section>
        </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ajax: {
                url: "{{ route('post.index') }}"
            },
            responsive: true,
            "columns": [
                {
                    data: 'id',
                    name: 'id',
                    orderable: false
                },
                {
                    data: 'title',
                    name: 'title',
                    orderable: false
                },
                {
                    data: 'tanggal',
                    name: 'tanggal',
                    orderable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        })
    });
</script>
@endsection