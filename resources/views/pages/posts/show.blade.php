@extends('layouts.app')

@section('title', 'Post')

@section('content')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
                <div class="section-header">
                    <h1>@yield('title')</h1>
                    <a href="{{ route('admin.post.index') }}" class="btn btn-warning ml-auto"><i class="fas fa-arrow-left"></i> Kembali</a>
                </div>
                @include('components.message')
                <div class="section-body">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-header post-header">
                                    <h4>{{ $post->title }}</h4>
                                </div>
                                <div class="card-body post-body">
                                    <p>{{ $post->description }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            @if($post->url)
                                            <iframe class="video" src="https://www.youtube.com/embed/{{ $post->url }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                            @else
                                            Tidak ada video
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            @if($post->document != null)
                                                <a href="{{ route('post.download', $post->id) }}" class="btn btn-primary btn-block"><i class="fas fa-download"></i> Download File</a>
                                            @else
                                            Tidak ada dokumen
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
@endsection
