@extends('layouts.auth')

@section('title','Login')

@section('content')

    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
                <strong>LMS <br> SMKN 1 PANJI</strong>
            </div>

            <div class="card card-primary">
                <br>     
                <div class="card-body">
                    @include('components.message')
                    <form method="POST" action="{{ route('admin.doLogin') }}" class="needs-validation" novalidate="">
                        @csrf
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" tabindex="1" autofocus>
                            @error('username')                                
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                {{-- Please fill in your username --}}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" tabindex="2">
                            @error('password')                                
                                <div class="invalid-feedback">
                                    <strong>{{ $message }}</strong>
                                {{-- please fill in your password --}}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-lg btn-block" style="border-radius: 25px" tabindex="4">
                                LOGIN
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="simple-footer">
                <p class="mb-0 text-center">Made with <i class="fas fa-heart text-danger"></i> by <a href="{{ route('author') }}">RPL 2018</a></p>
            </div>
        </div>
    </div>
    
@endsection