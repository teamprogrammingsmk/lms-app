@extends('layouts.app')

@section('title', 'Edit Post')

@section('content')
    <!-- Main Content -->
        <div class="main-content">
            <section class="section">
            <div class="section-header">
                <h1>@yield('title')</h1>
                <a href="{{ route('admin.post.index') }}" class="btn btn-warning ml-auto"><i class="fas fa-arrow-left"></i> Kembali</a>
            </div>
            @include('components.message')
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <form action="{{ route('admin.post.update', $post->id) }}" method="post" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-body">
                                    @method('put')
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Title" value="{{ $post->title }}" autocomplete="off" autofocus>
                                        @error('title')
                                            <div class="invalid-feedback">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control @error('description') is-invalid @enderror" name="description" placeholder="Description">{{ $post->description }}</textarea>
                                        @error('description')
                                            <div class="invalid-feedback">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input @error('document') is-invalid @enderror" name="document" placeholder="document" id="document" accept="image/png, image/jpeg, application/pdf">
                                            <label class="custom-file-label" for="document">Choose file</label>
                                        </div>
                                        @error('document')
                                            <div class="invalid-feedback">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control @error('url') is-invalid @enderror" name="url" placeholder="Youtube URL (Opsional)" value="https://www.youtube.com/watch?v={{ $post->url }}" autocomplete="off" autofocus>
                                        @error('url')
                                            <div class="invalid-feedback">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <select name="kelas" id="kelas" class="form-control @error('kelas') is-invalid @enderror">
                                            <option value=""> Pilih kelas</option>
                                            @foreach($pengajar as $pengajarKelas)
                                                <option value="{{ $pengajarKelas->kelas->id }}" @if($post->id_kelas == $pengajarKelas->kelas->id) selected @endif> {{ $pengajarKelas->kelas->nama_kelas }}</option>
                                            @endforeach
                                        </select>
                                        @error('kelas')
                                            <div class="invalid-feedback">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <select name="mapel" id="mapel" class="form-control @error('mapel') is-invalid @enderror">
                                            <option value=""> Pilih mapel</option>
                                            @foreach($pengajar as $pengajarMapel)
                                                <option value="{{ $pengajarMapel->mapel->id }}" @if($post->id_mapel == $pengajarMapel->mapel->id) selected @endif> {{ $pengajarMapel->mapel->nama_mapel}} | Kelas : {{ $pengajarMapel->mapel->tingkat  }}</option>
                                            @endforeach
                                        </select>
                                        @error('mapel')
                                            <div class="invalid-feedback">
                                                <strong>{{ $message }}</strong>
                                            </div>
                                        @enderror
                                    </div>
                                    <button class="btn btn-primary float-right"><i class="fas fa-save"></i> Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </section>
        </div>
@endsection