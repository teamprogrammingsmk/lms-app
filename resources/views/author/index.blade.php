<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    @include('components.head')
</head>

@section('title', 'Author')

<body>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <a href="{{ route('siswa.login') }}" class="btn btn-warning text-white mb-3"><i class="fas fa-arrow-left"></i> Kembali</a>
                    <div class="table-responsive">
                        <h2>About Us</h2>
                        <table class="table table-bordered table-striped mt-3">
                            <thead>
                                <tr>
                                    <th>Author</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <b>Name</b>: Alif Triadi Agung W<br />
                                    <b>Sosial Link</b>: <ul>
                                                    <li><a href="https://facebook.com/alftri.dev">Facebook</a></li>
                                                </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Name</b>: Vanessa Florentina Patricia<br />
                                    <b>Sosial Link</b>: <ul>
                                                    <li><a href="https://facebook.com/vanessa.florentina.1">Facebook</a></li>
                                                </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>

    </div>
    @include('components.script')
</body>

</html>