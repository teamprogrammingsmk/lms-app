<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(session('login')) {
        if(session('user')->role === 'admin') {
            return redirect(route('admin.post.index'));
        } else {
            return redirect(route('post.index'));
        }
    } else {
        return redirect(route('siswa.login'));
    }
});

Route::get('/author', function () {
    return view('author\index');
})->name('author');

Route::get('admin/login', 'AuthController@adminLogin')->name('admin.login');
Route::post('admin/doLogin', 'AuthController@adminDoLogin')->name('admin.doLogin');
Route::get('siswa/login', 'AuthController@siswaLogin')->name('siswa.login');
Route::post('siswa/doLogin', 'AuthController@siswaDoLogin')->name('siswa.doLogin');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::group(['middleware' => ['authLogin', 'isSiswa']], function() {
    Route::resource('post', 'PostController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['authLogin', 'isAdmin']], function() {
    Route::name('admin.')->group(function() {
        Route::resource('post', 'PostController');
    });
});

Route::get('post/download/{id}', 'PostController@download')->name('post.download');