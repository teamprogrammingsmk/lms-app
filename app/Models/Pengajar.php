<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Pengajar extends Model
{
    public static function getPengajar($pengajarId)
    {
        $response = Http::asForm()->post('http://182.253.101.61:8000/api/admin/pengajar', [
            'id' => $pengajarId,
        ]);

        if ($response->successful()) {
            $res = json_decode($response->body());

            return $res->data;
        }
    }
}
