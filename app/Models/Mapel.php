<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Mapel extends Model
{
    public static function getMapel($id)
    {
        $response = Http::asForm()->post('http://182.253.101.61:8000/api/admin/login', [
            'id' => $id,
        ]);

        $res = json_decode($response->body());
        return $res->data;
    }
}
