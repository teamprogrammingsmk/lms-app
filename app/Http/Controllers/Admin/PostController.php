<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User, App\Models\Post, App\Models\Pengajar;
use DataTables;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $posts = Post::query()->where('id_user', User::getUserBySession()->id)->orderBy('created_at', 'DESC');
            return DataTables::of($posts)
                ->addColumn('title', function ($posts) {
                    return \Illuminate\Support\Str::limit($posts->title, 8);
                })
                ->addColumn('tanggal', function ($posts) {
                    return \Carbon\Carbon::parse($posts->created_at)->translatedFormat('l, d F Y');
                })
                ->addColumn('action', function ($posts) {
                    $button = "<a href=" . route('admin.post.show', $posts->id) . " class='btn btn-info btn-sm'><i class='far fa-eye'></i></a>";
                    $button .= "&nbsp;";
                    $button .= "<a href=" . route('admin.post.edit', $posts->id) . " class='btn btn-warning btn-sm'><i class='fas fa-pencil-alt'></i></a>";
                    $button .= "&nbsp;";
                    $button .= "<form method='post' action='" . route('admin.post.destroy', $posts->id) . "' style='display: inline;'>" . method_field('DELETE') . csrf_field() . " <button type='submit' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button></form>";

                    return $button;
                })
                ->rawColumns(['title', 'tanggal', 'action'])
                ->make(true);
        }

        return view('pages.admin.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pengajar = Pengajar::getPengajar(session('user')->id);

        return view('pages.admin.posts.create', compact('pengajar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ytId = getYoutubeID($request->url);
        // Validasi
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'kelas' => 'required',
            'mapel' => 'required',
        ]);

        try {
            $file = $request->file('document');
            // Validate Image
            if ($file) {
                $request->validate([
                    'document' => 'max:20000|mimes:jpeg,jpg,png,pdf,ppt,pptx,xlx,xlsx,docx,doc'
                ]);

                $ext = $file->guessExtension();
                $filename = md5(time() . date('dmY H:i:s')) . '.' . $ext;
                $file->storeAs('docs', $filename, 'public');
            }

            Post::create([
                'title' => $request->title,
                'description' => $request->description,
                'document' => is_null($file) ? null : $filename,
                'url' => $ytId === 0 ? NULL : $ytId,
                'id_user' => User::getUserBySession()->id,
                'id_kelas' => $request->kelas,
                'id_mapel' => $request->mapel,
            ]);

            return redirect(route('admin.post.index'))->with('success', 'Materi berhasil di publish');
        } catch (\Exception $exc) {
            return redirect(route('admin.post.create'))->with('error', 'Terjadi kesalahan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('pages.admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $pengajar = Pengajar::getPengajar(session('user')->id);

        return view('pages.admin.posts.edit', compact('post', 'pengajar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ytId = getYoutubeID($request->url);
        // Validasi
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required',
            'kelas' => 'required',
            'mapel' => 'required',
        ]);

        try {
            $post = Post::findOrFail($id);

            $file = $request->file('document');
            // Validate Image
            if ($file) {
                $request->validate([
                    'document' => 'max:20000|mimes:jpeg,jpg,png,pdf'
                ]);

                $ext = $file->guessExtension();
                $filename = md5(time() . date('dmY H:i:s')) . '.' . $ext;
                $file->storeAs('docs', $filename, 'public');

                if ($post->document) {
                    unlink(public_path('storage/docs/' . $post->document));
                }
            }
            $post->update([
                'title' => $request->title,
                'description' => $request->description,
                'document' => is_null($file) ? $post->document : $filename,
                'url' => $ytId === 0 ? NULL : $ytId,
                'id_user' => User::getUserBySession()->id,
                'id_kelas' => $request->kelas,
                'id_mapel' => $request->mapel,
            ]);

            return redirect(route('admin.post.index'))->with('success', 'Materi berhasil di perbarui');
        } catch (\Exception $exc) {
            return redirect(route('admin.post.create'))->with('error', 'Terjadi kesalahan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect(route('admin.post.index'))->with('success', 'Materi berhasil di hapus');
    }
}
