<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function adminLogin(Request $request)
    {
        return view('pages.auth.admin.login');
    }

    public function adminDoLogin(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $response = Http::asForm()->post('http://182.253.101.61:8000/api/admin/login', [
            'username' => $request->username,
            'password' => $request->password,
        ]);

        if ($response->successful()) {
            $res = json_decode($response->body());

            session(['login' => true, 'user' => $res->data]);

            return redirect(route('post.index'));
        } elseif ($response->failed()) {
            $res = json_decode($response->body());

            return redirect('admin/login')->with('error', $res->data->message);
        } else {
            return redirect('admin/login')->with('error', 'Terjadi kesalahan');
        }
    }

    public function siswaLogin(Request $request)
    {
        return view('pages.auth.siswa.login');
    }

    public function siswaDoLogin(Request $request)
    {
        $request->validate([
            'nis' => 'required',
            'password' => 'required'
        ]);

        $response = Http::asForm()->post('http://182.253.101.61:8000/api/siswa/login', [
            'nis' => $request->nis,
            'password' => $request->password,
        ]);

        if ($response->successful()) {
            $res = json_decode($response->body());

            session(['login' => true, 'user' => $res->data]);

            return redirect(route('post.index'));
        } elseif ($response->failed()) {
            $res = json_decode($response->body());

            return redirect('siswa/login')->with('error', $res->data->message);
        } else {
            return redirect('siswa/login')->with('error', 'Terjadi kesalahan');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->forget(['login', 'user']);
        $request->session()->flush();
        return redirect('/')->with('success', 'Anda berhasil logout');
    }
}
