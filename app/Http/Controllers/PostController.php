<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User, App\Models\Post, App\Models\Siswa, App\Models\Mapel;
use Illuminate\Support\Facades\Storage;
use DataTables;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $posts = Post::query()->where('id_kelas', Siswa::getBySession()->kelas->kelas_id)->orderBy('created_at', 'DESC');
            
            return DataTables::of($posts)
                            ->addColumn('post', function($posts) {
                                return \Illuminate\Support\Str::limit($posts->post, 8);
                            })
                            ->addColumn('tanggal', function($posts) {
                                return \Carbon\Carbon::parse($posts->created_at)->translatedFormat('l, d F Y');
                            })
                            ->addColumn('action', function($jurnal) {
                                $button = "<a href=" . route('post.show', $jurnal->id) . " class='btn btn-info btn-sm'><i class='far fa-eye'></i></a>";
                                return $button;
                            })
                            ->rawColumns(['post', 'tanggal', 'action'])
                            ->make(true);
        }

        return view('pages.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kelas_id = Siswa::getBySession()->kelas->kelas_id;
        $post = Post::findOrFail($id);

        if($post->id_kelas != $kelas_id) {
            return redirect(route('post.index'))->with('error', 'Terjadi kesalahan');
        }
        return view('pages.admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Download Post File
    */

    public function download($id) {
        $post = Post::findOrFail($id);

        return Storage::disk('local')->download('public/docs/'.$post->document);
    }
}
